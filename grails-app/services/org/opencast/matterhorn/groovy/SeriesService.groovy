package org.opencast.matterhorn.groovy

import groovy.xml.MarkupBuilder
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method

import org.apache.http.util.EntityUtils
import org.opencast.matterhorn.groovy.transformer.SeriesJsonTransformer
import org.opencast.matterhorn.groovy.transformer.SeriesXmlTransformer

class SeriesService {

	def grailsApplication
	
	def get(identifier) {
		def series = [:]
		def http = new HTTPBuilder(grailsApplication.config.matterhorn.core.url + "/series/" + identifier + ".json")
		http.auth.basic grailsApplication.config.matterhorn.core.username, grailsApplication.config.matterhorn.core.password
		http.request(Method.GET, groovyx.net.http.ContentType.JSON) {
			headers.'X-Requested-Auth' = 'Digest'
			response.success = {resp, json ->
				series = SeriesJsonTransformer.transform(json)
				log.info(resp.statusLine.statusCode + " Found series " + series)
			}
			response.failure = {resp, json ->
				log.error(resp.statusLine.statusCode + " Failed to get series " + identifier)
			}
		}
		return series
	}
	
	def find(query) {
		def result = []
		def http = new HTTPBuilder(grailsApplication.config.matterhorn.core.url + "/series/series.json?" + query.collect { it }.join('&'))
		http.auth.basic grailsApplication.config.matterhorn.core.username, grailsApplication.config.matterhorn.core.password
		http.request(Method.GET, groovyx.net.http.ContentType.JSON) {
			headers.'X-Requested-Auth' = 'Digest'
			response.success = {resp, json ->
				json.catalogs.each {
					result << SeriesJsonTransformer.transform(it)
				}
				def count = json.catalogs.size()
				log.info(resp.statusLine.statusCode + " Found $count of $json.totalCount series for query " + query)
			}
			response.failure = {resp, json ->
				log.error(resp.statusLine.statusCode + " Failed to find series for query " + query)
			}
		}
		return result
	}
	
    def update(series) {
		def result = [:]
		def dcWriter = new StringWriter()
		def dcXml = new MarkupBuilder(dcWriter)
		dcXml.'dublincore'(
			'xmlns': 'http://www.opencastproject.org/xsd/1.0/dublincore/', 
			'xmlns:xsi':'http://www.w3.org/2001/XMLSchema-instance', 
			'xsi:schemaLocation':'http://www.opencastproject.org http://www.opencastproject.org/schema.xsd',
			'xmlns:dc':'http://purl.org/dc/elements/1.1/',
			'xmlns:dcterms': 'http://purl.org/dc/terms/',  
			'xmlns:oc':'http://www.opencastproject.org/matterhorn/') {
			dcXml.'dcterms:identifier'(series.identifier)
			dcXml.'dcterms:title'(series.title)
			dcXml.'dcterms:description'(series.description)
			dcXml.'dcterms:isPartOf'(series.isPartOf)
			dcXml.'dcterms:creator'(series.creator)
			dcXml.'dcterms:spatial'(series.spatial)
			dcXml.'dcterms:temporal'(series.temporal)
		}
			
		def aclWriter = new StringWriter()
		def aclXml = new MarkupBuilder(aclWriter)
		aclXml.'acl'('xmlns': 'org.opencastproject.security') {
			aclXml.'ace'() {
				aclXml.'role'('ROLE_ANONYMOUS')
				aclXml.'action'('read')
				aclXml.'allow'('true')
			}
		}
		
		def http = new HTTPBuilder(grailsApplication.config.matterhorn.core.url + "/series")
		http.auth.basic 'matterhorn_system_account', 'CHANGE_ME'
		http.request(Method.POST, groovyx.net.http.ContentType.URLENC) {
			headers.'X-Requested-Auth' = 'Digest'
			headers.'Accept' = 'application/xml'
			body = [
				series: dcWriter.toString(),
				acl: aclWriter.toString()
			]
			response.success = {resp ->
				def xml = new XmlSlurper().parseText(EntityUtils.toString(resp.entity)).declareNamespace(dcterms:'http://purl.org/dc/terms/')
				result = SeriesXmlTransformer.transform(xml)
				log.info(resp.statusLine.statusCode + " Updated series " + result)
			}
			response.failure = {resp ->
				log.error(resp.statusLine.statusCode + " Failed to update series " + series.identifier)
			}
		}
		return result
    }
	
	def delete(identifier) {
		def http = new HTTPBuilder(grailsApplication.config.matterhorn.core.url + "/series/" + identifier)
		http.auth.basic grailsApplication.config.matterhorn.core.username, grailsApplication.config.matterhorn.core.password
		http.request(Method.DELETE, groovyx.net.http.ContentType.JSON) {
			headers.'X-Requested-Auth' = 'Digest'
			response.success = {resp, json ->
				log.info(resp.statusLine.statusCode + " Deleted series " + identifier)
			}
			response.failure = {resp, json ->
				log.error(resp.statusLine.statusCode + " Failed to delete series " + identifier)
			}
		}
	}
	
}

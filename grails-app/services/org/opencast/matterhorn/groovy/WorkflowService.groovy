package org.opencast.matterhorn.groovy

import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method

import org.opencast.matterhorn.groovy.utils.GroovyhornUtils

class WorkflowService {

	def grailsApplication
	
	def instances(query) {
		def result = []
		def http = new HTTPBuilder(grailsApplication.config.matterhorn.core.url + "/workflow/instances.json?" + query.collect { it }.join('&'))
		http.auth.basic grailsApplication.config.matterhorn.core.username, grailsApplication.config.matterhorn.core.password
		http.request(Method.GET, groovyx.net.http.ContentType.JSON) {
			headers.'X-Requested-Auth' = 'Digest'
			response.success = {resp, json ->
				def instances = json.workflows.workflow
				result << (instances ?: [])
				result = result.flatten()
				log.info(resp.statusLine.statusCode + " Found $result.size of $json.workflows.totalCount workflow instances for query " + query)
			}
			response.failure = {resp, json ->
				log.error(resp.statusLine.statusCode + " Failed to find workflow instances for query " + query)
			}
		}
		return result
	}
	
	def stop(workflowInstanceId) {
		def result = false
		def http = new HTTPBuilder(grailsApplication.config.matterhorn.core.url + "/workflow/stop")
		http.auth.basic grailsApplication.config.matterhorn.core.username, grailsApplication.config.matterhorn.core.password
		http.request(Method.POST, groovyx.net.http.ContentType.URLENC) {
			headers.'X-Requested-Auth' = 'Digest'
			headers.'Accept' = 'text/xml'
			body = [
				id: workflowInstanceId
			]
			response.success = {resp ->
				result = true
				log.info(resp.statusLine.statusCode + " Stopped workflow " + workflowInstanceId)
			}
			response.failure = {resp ->
				result = false
				log.error(resp.statusLine.statusCode + " Failed to stop workflow " + workflowInstanceId)
			}
		}
		return result
	}
	
}

package org.opencast.matterhorn.groovy

import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method

import org.opencast.matterhorn.groovy.utils.GroovyhornUtils

class SearchService {

	def grailsApplication
	
	def episode(query) {
		def result = []
		def http = new HTTPBuilder(grailsApplication.config.matterhorn.core.url + "/search/episode.json?" + query.collect { it }.join('&'))
		http.auth.basic grailsApplication.config.matterhorn.core.username, grailsApplication.config.matterhorn.core.password
		http.request(Method.GET, groovyx.net.http.ContentType.JSON) {
			headers.'X-Requested-Auth' = 'Digest'
			response.success = {resp, json ->
				def results = json["search-results"]
				def episodes = results.result
				result << (episodes ?: [])
				result = result.flatten().collect() { it.mediapackage }
				log.info(resp.statusLine.statusCode + " Found $result.size of $results.total episodes for query " + query)
			}
			response.failure = {resp, json ->
				log.error(resp.statusLine.statusCode + " Failed to find episodes for query " + query)
			}
		}
		return result
	}
	
	def delete(mediaPackageId) {
		def result = false
		def http = new HTTPBuilder(grailsApplication.config.matterhorn.core.url + "/search/" + mediaPackageId)
		http.auth.basic grailsApplication.config.matterhorn.core.username, grailsApplication.config.matterhorn.core.password
		http.request(Method.DELETE, groovyx.net.http.ContentType.JSON) {
			headers.'X-Requested-Auth' = 'Digest'
			response.success = {resp, json ->
				result = true
				log.info(resp.statusLine.statusCode + " Deleted mediapackage from the search index " + mediaPackageId)
			}
			response.failure = {resp, json ->
				result = false
				log.error(resp.statusLine.statusCode + " Failed to delete mediapackage from the search index " + mediaPackageId)
			}
		}
		return result
	}
	
}

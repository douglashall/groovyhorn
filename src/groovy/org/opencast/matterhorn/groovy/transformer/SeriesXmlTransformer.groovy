package org.opencast.matterhorn.groovy.transformer


class SeriesXmlTransformer {

	public static Map transform(xml) {
		def series = [:]
		series.identifier = xml.'dcterms:identifier'.text()
		series.title = xml.'dcterms:title'.text()
		series.description = xml.'dcterms:description'.text()
		return series
	}
	
}

package org.opencast.matterhorn.groovy.transformer


class SeriesJsonTransformer {

	public static Map transform(json) {
		def series = [:]
		def dc = json['http://purl.org/dc/terms/']
		series.identifier = dc.identifier[0].value
		series.title = dc.title ? dc.title[0].value : ''
		series.description = dc.description ? dc.description[0].value : ''
		series.isPartOf = dc.isPartOf ? dc.isPartOf[0].value : ''
		return series
	}
	
}

package org.opencast.matterhorn.groovy.utils

import java.text.SimpleDateFormat

class GroovyhornUtils {

	def static String matterhornDateFormatString = "yyyy-MM-dd'T'hh:mm:ss'Z'"
	def static SimpleDateFormat matterhornDateFormat = new SimpleDateFormat(matterhornDateFormatString)
	
	def static Date parseDate(String date) {
		try {
			return matterhornDateFormat.parse(date)
		} catch (Exception e) {}
		return null
	}
	
	def static String formatDate(Date date) {
		return matterhornDateFormat.format(date)
	}
	
}
